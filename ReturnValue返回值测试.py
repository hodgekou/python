def TestReturnValue(a, b, c, d):
    return a + b, c * d

num1, num2 = TestReturnValue(1, 2, 3, 4)

print(num1, num2)

r = TestReturnValue(1, 2, 3, 4)
print(r)