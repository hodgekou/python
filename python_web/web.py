import socket
from multiprocessing import Process

HTML_ROOT_DIR = ""


def handle_client(client_socket):
    request_data = client_socket.recv(1024)
    print("request data:", request_data)
    response_start_line = "HTTP/1.0 200 OK\r\n"
    response_headers = "Server: My server\r\n"
    response_body = "hello world"
    response = response_start_line + \
               "\r\n" + response_body
    print("response data:", response)

    client_socket.send(bytes(response, "utf-8"))
    client_socket.close()


if __name__ == "__main__":
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(("127.0.0.1", 8080))
    server_socket.listen(128)
    while True:
        client_socket, client_address = server_socket.accept()
        handle_client_process = Process(target=handle_client, args=(client_socket,))
        handle_client_process.start()
        client_socket.close()




