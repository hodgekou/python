'''
def add(x, y, f):
    return f(x) + f(y)

x = -5
y = 6
f = abs

print(add(x, y, f))



def f(x):
    return x * x
r = list(map(f, [1, 2, 3, 4, 5, 6, 7, 8, 9]))

print(r)



def add(x, y):
    return x + y
print(reduce(add, [1, 3, 5, 7, 9]))

from functools import reduce

def fn(x, y):
    return x * 10 + y

print(reduce(fn,[1, 3, 4, 5, 6]))


print("字符串转整形")
from functools import reduce
def fn(x, y):
    return x * 10 + y

def char2num(s):
    digits = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}
    return digits[s]

L = reduce(fn, map(char2num, '13579'))
print(isinstance(L,int))

'''


from functools import reduce
def add(x, y):
    return x * y
L = reduce(add, [1, 3, 5, 7, 9])
print(L)
