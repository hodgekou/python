def add_end_Right(L=None):
    if L is None:
        L = []
    L.append('END')
    return L


def add_end_False(L=[]):
    L.append('END')
    return L


print("-------------")
print(add_end_Right())
print(add_end_Right())
print(add_end_Right())
print(add_end_Right())

print("-------------")
print(add_end_False())
print(add_end_False())
print(add_end_False())
print(add_end_False())
