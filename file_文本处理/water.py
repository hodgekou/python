#!/usr/bin/env python
# coding=utf-8

#e10.3CalThreeKingdoms.py


import jieba
excludes = {}#{"将军","却说","丞相"}
# txt = open("threekingdom.txt", "rb", encoding='utf-8').read()
txt = open("water.txt", "rb").read()
words  = jieba.lcut(txt)
counts = {}
for word in words:
    if len(word) == 1:  #排除单个字符的分词结果
        continue
    else:
        counts[word] = counts.get(word,0) + 1
items = list(counts.items())
items.sort(key=lambda x:x[1], reverse=True) 
for i in range(30):
    word, count = items[i]
    print ("{0:<10}{1:>5}".format(word, count))
