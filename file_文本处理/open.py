#!/usr/bin/env python
# coding=utf-8

##open and close 
#textFile = open("open.txt", "rt")
#print(textFile.readline())
#textFile.close()

#binFile = open("open.txt","rb")
#print(binFile.readline())
#binFile.close()

##open and print
#fname = input("Enter the file name")
#fo = open(fname, "r")
#for line in fo:
#    print(line)
#fo.close

##open and write

fo = open("write.txt","w+")
ls = ["I ","am ", "Hodge"]
fo.writelines(ls)
for line in fo:
    print(line)
fo.close()








