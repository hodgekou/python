#!/usr/bin/env python
# coding=utf-8

import jieba
import wordcloud

f = open("threekingdom.txt","rb")
t = f.read()
f.close()
ls = jieba.lcut(t)
txt = " ".join(ls)
w = wordcloud.WordCloud(    font_path = "NotoSerifCJK-Bold.ttc",\
                        width = 1000,height = 700,background_color = "white",\
                       )

w.generate(txt)
w.to_file("gr.png")
