"""
import json

print("-----------------------------")
d = dict(name='Bob', age=20, score=80)
P = json.dumps(d)
print(P)

print("-----------------------------")
json_str = '{"age":20, "score":80, "name":"Bob"}'
P = json.loads(json_str)
print(P)

print("-----------------------------")
class Student(object):
    def __init__(self, name, age, score):
        self.name = name
        self.age = age
        self.score = score

def student2dict(std):
    return {
        'name': std.name,
        'age': std.age,
        'score': std.score
    }

s = Student("Bob", 20, 88)
print(json.dumps(s, default=student2dict))

"""

# 在上面的例子如果有一个teacher类就没法序列化为json。处理方法
import  json
class Student(object):
    def __init__(self, name, age, score):
        self.name = name
        self.age = age
        self.score = score

def student2dict(std):
    return {
        'name': std.name,
        'age': std.age,
        'score': std.score
    }


s = Student("Bob", 20, 88)
#print(json.dumps(s, default=student2dict))
print(json.dumps(s, default=lambda  obj: obj.__dict__))

