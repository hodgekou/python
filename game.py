#!/usr/bin/env python
# coding=utf-8

import random

number = random.randint(1,100)
num = 0
print(number)

def fun1():
    global num
    print("Enter :",end='')
    num = input()
    if num == '0':
        print("Exit")
        exit(0)
    int(num)

def fun2():
    global num
    global number
    num = int(num)
    number = int(number)

name = input("Your name is ?:")
print('Hi,'+ name + '! Now I have a number in range(0,100)')
print("You have 5 changes to guess it")

i = 0
while i < 5:
    i += 1
    fun1()
    fun2()
    if num == number:
        print("Very Good")
        break
    elif num < number:
        print("Small")
    elif num > number:
        print("Biger")

