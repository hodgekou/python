'''


L = ['Hello', 'World', '!']


print("-------1.一个一个取-------")
print(L[0])
print(L[1])
print(L[2])


print("-------2.开辟一个新列表把内容存进去-------")
r = []
for i in range(3):
    r.append(L[i])

print(r)


print("-------3.切片操作-------")
print("L[0:3]", L[0:3])
print("L[:3]", L[:3])
print("L[1:3]", L[1:3])
print("L[-1]", L[-1])
print("L[-2:]", L[-2:])
print("L[-2:-1]", L[-2:-1])


print("_____________切片操作详解——————————————————————")
L = list(range(1, 100))
print(L)

print(L[:10])
print(L[5:10])
print(L[-10])
print(L[-10:])
print(L[:-80])
print(L[10:-80])


print("前10个数每隔2个取一个")
print(L[::])
print(L[:10:2])
print("所有数每隔5个取一个")
print(L[::5])

def trim(s):
    length = len(s) - 1
    if length < 0:
        return ''
    last = length
    while s[ length ] == ' ' :
        length -= 1
        last = length
        if length < 0:
            return ''
    first = 0
    while s[first] == ' ':
        first += 1
    last += 1
    l = s[first:last]
    return l


if trim('hello  ') != 'hello':
    print('测试失败!')
elif trim('  hello') != 'hello':
    print('测试失败!')
elif trim('  hello  ') != 'hello':
    print('测试失败!')
elif trim('  hello  world  ') != 'hello  world':
    print('测试失败!')
elif trim('    ') != '':
    print('测试失败!')
elif trim('') != '':
    print('测试失败!')
else:
    print('测试成功!')



def findMinAndMax(L):
    if len(L) == 0:
        return None, None

    max, min = L[0], L[0]

    for i in L:
        if min > i:
            min = i
        if max < i:
            max = i

    return min, max


if findMinAndMax([]) != (None, None):
    print('测试失败!')
elif findMinAndMax([7]) != (7, 7):
    print('测试失败!')
elif findMinAndMax([7, 1]) != (1, 7):
    print('测试失败!')
elif findMinAndMax([7, 1, 3, 9, 5]) != (1, 9):
    print('测试失败!')
else:
    print('测试成功!')






L = ['Hello', 'World', 'IBM', 'Apple']
L2 = [s.lower() for s in L if isinstance(s, str)]
print(L2)

'''
